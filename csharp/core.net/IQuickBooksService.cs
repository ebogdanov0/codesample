﻿using System.Threading.Tasks;

namespace Quickbooks
{
    public interface IQuickBooksService
    {
        Task Sync();
        Task SyncFromQuickbooks();
        Task SyncToQuickbooks();

        string GetRedirectURL();

        Task GetToken(string code, string realmId);

        Task RenewToken();
    }
}