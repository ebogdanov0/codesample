﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using DataLayer.Entities;
using Intuit.Ipp.OAuth2PlatformClient;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.Security;
using Intuit.Ipp.Exception;

namespace Quickbooks
{
    public class QuickBooksService : IQuickBooksService
    {
        DBContext _db;

        OAuth2Client oAuth2Client;

        private static string clientId = "Q0yebRJgPF0R5DqdZVzOpeAe4B1pFgdmBSaFcD0eVLHDMDH7r9";
        private static string clientSecret = "OInt0KjDeruNFAnah0kPzkwzmtElo36FvhBUSUv2";
        private static string redirectUrl = "http://localhost:5000/api/v1/tps/Quickbooks/getToken";
        private static string environment = "sandbox";
        private static string minorApiVersion = "23";

        public QuickBooksService(DBContext db)
        {
            _db = db;

            oAuth2Client = new OAuth2Client(clientId, clientSecret, redirectUrl, environment);
        }

        public string GetRedirectURL()
        {
            var state = Guid.NewGuid().ToString("N");

            List<OidcScopes> scopes = new List<OidcScopes>();
            scopes.Add(OidcScopes.Accounting);

            return oAuth2Client.GetAuthorizationURL(scopes, state);
        }

        public async System.Threading.Tasks.Task GetToken(string code, string realmId)
        {
            TokenResponse tokenResponse = await oAuth2Client.GetBearerTokenAsync(code);

            SaveToken(realmId, tokenResponse);
        }

        public async System.Threading.Tasks.Task RenewToken()
        {
            var tokenResponse = await RefreshToken();
        }
        private async Task<TokenResponse> RefreshToken()
        {
            Qbsettings qbToken = GetDBSettings();

            TokenResponse tokenResponse = await oAuth2Client.RefreshTokenAsync(qbToken.RefreshToken);

            // TODO Here we need somehow to block idea that this code can be used in concurent sessions
            // for this account

            lock (qbToken)
            {
                // If we've expired access token - or will expire soon = renew
                if (tokenResponse.AccessToken != null && tokenResponse.RefreshToken != null)
                {
                    SaveToken(qbToken.RealmId, tokenResponse);
                }
            }

            return tokenResponse;
        }

        public async Task<CompanyInfo> GetCompanyInfo()
        {
            // TODO This construction should be refactored
            try
            {
                ServiceContext serviceContext = InitServiceContext();

                // Create a QuickBooks QueryService using ServiceContext
                QueryService<CompanyInfo> querySvc = new QueryService<CompanyInfo>(serviceContext);
                CompanyInfo companyInfo = querySvc.ExecuteIdsQuery("SELECT * FROM CompanyInfo").FirstOrDefault();

                return companyInfo;
            }
            catch (Exception e)
            {
                if (e.Message == "Unauthorized-401")
                {
                    await RefreshToken();

                    ServiceContext serviceContext = InitServiceContext();
                    QueryService<CompanyInfo> querySvc = new QueryService<CompanyInfo>(serviceContext);
                    CompanyInfo companyInfo = querySvc.ExecuteIdsQuery("SELECT * FROM CompanyInfo").FirstOrDefault();

                    return companyInfo;
                }

                throw e;
            }
        }

        public ReadOnlyCollection<AccountList> GetAccounts()
        {
            // TODO This should be refactored 
            try
            {
                ServiceContext serviceContext = InitServiceContext();

                // Create a QuickBooks QueryService using ServiceContext
                QueryService<AccountList> querySvc = new QueryService<AccountList>(serviceContext);
                var accountsList = querySvc.ExecuteIdsQuery("SELECT * FROM Accounts Where Active = \"TRUE\"");

                return accountsList;
            }
            catch (Exception e)
            {
                if (e.Message == "Unauthorized-401")
                {
                    RefreshToken().Wait();

                    ServiceContext serviceContext = InitServiceContext();

                    // Create a QuickBooks QueryService using ServiceContext
                    QueryService<AccountList> querySvc = new QueryService<AccountList>(serviceContext);
                    var accountsList = querySvc.ExecuteIdsQuery("SELECT * FROM Accounts Where Active = \"TRUE\"");

                    return accountsList;
                }

                throw e;
            }
        }

        public async System.Threading.Tasks.Task SyncToQuickbooks()
        {
            await Sync();
        }
        public async System.Threading.Tasks.Task SyncFromQuickbooks()
        {

            List<IEntity> entityList = new List<IEntity>();
            entityList.Add(new Account());
            entityList.Add(new Customer());


            // CDC service approach
            // This code should be refactored
            try
            {
                ServiceContext serviceContext = InitServiceContext();
                var DataService = new DataService(serviceContext);

                // We should use this value from table
                IntuitCDCResponse CDCResponse = DataService.CDC(entityList, DateTime.Now.AddDays(-360));

                ProcessCDCDataResponse(CDCResponse);
            }
            catch (Exception e)
            {
                if (e.Message == "Unauthorized-401")
                {
                    await RefreshToken();

                    ServiceContext serviceContext = InitServiceContext();
                    var DataService = new DataService(serviceContext);

                    IntuitCDCResponse CDCResponse = DataService.CDC(entityList, DateTime.Now.AddDays(-360));

                    ProcessCDCDataResponse(CDCResponse);
                }

                throw e;
            }
        }

        protected bool ProcessCDCDataResponse(IntuitCDCResponse CDCResponse)
        {
            Entities.Manager manager = new Entities.Manager(_db);

            // Read data from response
            foreach (string key in CDCResponse.entities.Keys)
            {
                var entitesList = CDCResponse.entities[key];
                foreach (IntuitEntity entity in entitesList)
                {
                    Console.WriteLine(entity.ToString());
                    manager.Import(entity);
                }
            }


            qbToken.LastSyncDate = DateTime.Now;
            _db.Qbsettings.Update(qbToken);

            return true;
        }

        public async System.Threading.Tasks.Task Sync()
        {
            var data = await _db.Payments.ToListAsync();

            Links links = new Links();

            foreach (Payments entity in data)
            {
                // TODO Function callback
                try
                {
                    Adapters.Quickbooks.Builder QuickbooksBuilder = new Adapters.Quickbooks.Builder(entity, links);
                    QuickbooksBuilder.BuildCustomer();
                    QuickbooksBuilder.BuildItems();
                    QuickbooksBuilder.BuildInvoices();

                    // qbEntity.save();
                }
                catch (Exception e)
                {
                    // load new refresh token, 
                    // retry function
                }
                // Check if we've exported this entity
                // If not - call factory
                // Try export
                // Got error?
                // 401 - refresh key first. Failed? - Log
                // Token mismatch? - Download new token from QBO and insert into database
                //                 - Try again to export this enitity
            }

        }

        private Qbsettings GetDBSettings()
        {
            return _db.Qbsettings.FirstOrDefault();
        }

        private bool SaveToken(string realmId, TokenResponse tokenResponse)
        {
            Qbsettings record = GetDBSettings();

            if (record != null)
            {
                record.RealmId = realmId;
                record.AccessToken = tokenResponse.AccessToken;
                record.RefreshToken = tokenResponse.RefreshToken;
                record.RefreshTokenExpire = DateTime.Now.AddSeconds(tokenResponse.RefreshTokenExpiresIn);

                _db.Qbsettings.Update(record);
            }
            else
            {
                _db.Qbsettings.Add(new Qbsettings
                {
                    RealmId = realmId,
                    AccessToken = tokenResponse.AccessToken,
                    RefreshToken = tokenResponse.RefreshToken,
                    RefreshTokenExpire = DateTime.Now.AddSeconds(tokenResponse.RefreshTokenExpiresIn)
                });
            }
            _db.SaveChanges();

            return true;
        }

        protected ServiceContext InitServiceContext() {
            Qbsettings qbToken = GetDBSettings();

            OAuth2RequestValidator oauthValidator = new OAuth2RequestValidator(qbToken.AccessToken);

            // Create a ServiceContext with Auth tokens and realmId
            ServiceContext serviceContext = new ServiceContext(qbToken.RealmId, IntuitServicesType.QBO, oauthValidator);
            serviceContext.IppConfiguration.MinorVersion.Qbo = minorApiVersion;

            return serviceContext;
        }
    }
}
