﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DataLayer;

namespace Quickbooks.Entities
{
    class Version
    {
        DBContext _db;

        public Version(DBContext db)
        {
            _db = db;
        }

        public Guid FindLinkId(Intuit.Ipp.Data.IntuitEntity quickBooksEntity)
        {
            int entityId      = Int32.Parse(quickBooksEntity.Id);
            string entityType = GetQuickBooksEntityType(quickBooksEntity);
            int entityTypeId  = GetEntityTypeId(entityType);

            DataLayer.Entities.Links linksRecord = _db.Links.Where(
                c => c.ThirdPartyEntityId == entityId && c.TypeEnum == entityTypeId
            ).SingleOrDefault();

            if (linksRecord == null)
            {   
                return new Guid(entityId, (short) entityTypeId, 0, new byte[8]);
            }

            return linksRecord.TpslinkId;
        }

        public bool HasLink(Intuit.Ipp.Data.IntuitEntity quickBooksEntity)
        {
            int entityId = Int32.Parse(quickBooksEntity.Id);
            string entityType = GetQuickBooksEntityType(quickBooksEntity);
            int entityTypeId = GetEntityTypeId(entityType);

            int recordsCount = _db.Links.Where(
                c => c.ThirdPartyEntityId == entityId && c.TypeEnum == entityTypeId
            ).Count();

            return recordsCount > 0;
        }

        public void UpdateLink(Intuit.Ipp.Data.IntuitEntity quickBooksEntity, Guid dbGuid)
        {
            int entityId      = Int32.Parse(quickBooksEntity.Id);
            string entityType = GetQuickBooksEntityType(quickBooksEntity);
            int entityTypeId  = GetEntityTypeId(entityType);

            if (HasLink(quickBooksEntity))
            {
                // Update ids, versions

                var selectRecord = _db.Links.Where(
                    c => c.ThirdPartyEntityId == entityId && c.TypeEnum == entityTypeId
                ).Take(1).OfType<DataLayer.Entities.Links>();

                DataLayer.Entities.Links linksRecord = (DataLayer.Entities.Links)selectRecord;

                linksRecord.ThirdPartyEntityId = entityTypeId;
                linksRecord.ThirdPartyLinkId = quickBooksEntity.SyncToken;
                linksRecord.TpslinkId = dbGuid;
                linksRecord.TpsentityId = 13;
                linksRecord.StatusEnum = 1; // @todo Need to verify if this correct or not ???
                linksRecord.LastSynch = DateTime.Now;

                _db.Links.Update(linksRecord);
            }
            else
            {
                // Add ids, version
                DataLayer.Entities.Links linksRecord = new DataLayer.Entities.Links();

                linksRecord.ThirdPartyEntityId = entityTypeId;
                linksRecord.ThirdPartyLinkId   = quickBooksEntity.SyncToken;
                linksRecord.TpslinkId          = dbGuid;
                linksRecord.TypeEnum           = entityTypeId;
                linksRecord.LastSynch          = DateTime.Now;
                linksRecord.StatusEnum         = 1; // ???
                linksRecord.TpsentityId        = 13;

                _db.Links.Add(linksRecord);
            }

            _db.SaveChanges();
        }

        public string GetQuickBooksEntityType(Intuit.Ipp.Data.IntuitEntity quickBooksEntity)
        {
            string quickBooksEntityType = quickBooksEntity.GetType().ToString();

            return quickBooksEntityType;
        }

        protected int GetEntityTypeId(string entityType)
        {
            switch (entityType)
            {
                case "Intuit.Ipp.Data.Customer":
                    return 1;

                case "Intuit.Ipp.Data.Account":
                    return 2;

                case "Intuit.Ipp.Data.Invoice":
                    return 3;
            }

            throw new NotImplementedException(entityType + " do not have matched id");
        }
    }
}
