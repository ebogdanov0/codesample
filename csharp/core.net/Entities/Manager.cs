﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Quickbooks.Entities;

namespace Quickbooks.Entities
{
    class Manager
    {
        DataLayer.DBContext _db;

        protected Version _version;

        public Manager(DataLayer.DBContext db)
        {
            _db = db;
            _version = new Version(_db);
        }

        public bool Import(Intuit.Ipp.Data.IntuitEntity quickBooksEntity)
        {
            string quickbBooksEntityType = _version.GetQuickBooksEntityType(quickBooksEntity);
            Console.WriteLine(quickbBooksEntityType);

            Import.IEntity converterInstance = getInstanceConverter(quickbBooksEntityType);

            converterInstance.SetData(quickBooksEntity);
            converterInstance.Convert();

            converterInstance.Save();
           
            return true;
        }

        private Import.IEntity getInstanceConverter(string quickbBooksEntityType)
        {
            switch (quickbBooksEntityType)
            {
                case "Intuit.Ipp.Data.Invoice":
                    return new Import.Invoice(this._db);
                    
                case "Intuit.Ipp.Data.Customer":
                    return new Import.Customer(this._db);

                case "Intuit.Ipp.Data.Account":
                    return new Import.Account(this._db);

                case "Intuin.Ipp.Data.Employee":
                    return new Import.Employee(this._db);
            }

            throw new NotImplementedException("Not implemented");
        }
    }
}
