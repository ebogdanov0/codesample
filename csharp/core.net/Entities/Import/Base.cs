﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DataLayer;

namespace Quickbooks.Entities.Import
{
    abstract class Base : IEntity
    {
        public string DestEntityType { get; set; }

        protected DBContext _db;

        protected Version _version;

        protected Guid _officeId;
        
        public Base(DBContext db)
        {
            _db = db;
            _version = new Version(_db);

            var office = _db.Offices.SingleOrDefault();

            if (office == null)
            {
                throw new Exception("Organization setup is not finished");
            }

            _officeId = office.Id;
        }
        abstract public void Convert();

        abstract public Guid Save();

        abstract public void SetData(Intuit.Ipp.Data.IntuitEntity quickbooksEntity);
    }
}
