﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DataLayer;

namespace Quickbooks.Entities.Import
{
    class Employee : Base
    {
        Intuit.Ipp.Data.Employee sourceData;

        DataLayer.Entities.Employees dbRecord;

        public Employee(DBContext db) : base(db)
        {
        }

        public override void SetData(Intuit.Ipp.Data.IntuitEntity quickBooksEntity)
        {
            sourceData = (Intuit.Ipp.Data.Employee)quickBooksEntity;
        }

        public override void Convert()
        {
            dbRecord = new DataLayer.Entities.Employees();

            dbRecord.StandardRate = sourceData.BillRate;
            dbRecord.HireDate     = sourceData.HiredDate;

            // Search client
            dbRecord.PersonOrCompanyId = getClientId();
            
            dbRecord.ModifyVersion = System.Text.Encoding.ASCII.GetBytes(sourceData.SyncToken);
        }

        public override Guid Save()
        {
            // If found
            if (_version.HasLink(sourceData))
            {
                _db.Employees.Update(dbRecord);
            }
            else
            {
                _db.Employees.Add(dbRecord);
            }
            
            return dbRecord.Id;
        }

        protected Guid getClientId()
        {
            var personOrCompany = _db.PersonOrCompany.Where(z =>
                    z.FirstName == sourceData.GivenName &&
                    z.LastName == sourceData.FamilyName &&
                    z.CompanyName == sourceData.CompanyName
            ).Single();

            if (personOrCompany == null)
            {
                // If not found - insert data
                Entities.Import.Customer importCustomer = new Entities.Import.Customer(_db);
                importCustomer.Convert();

                return importCustomer.Save();
            }

            return personOrCompany.Id;
        }
    }
}
