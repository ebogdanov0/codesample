﻿using System;

namespace Quickbooks.Entities.Import
{
    interface IEntity
    {
        Guid Save();

        void SetData(Intuit.Ipp.Data.IntuitEntity quickbooksEntity);

        void Convert();
    }
}
