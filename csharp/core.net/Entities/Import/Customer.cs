﻿using System;
using System.Linq;
using DataLayer;
using DataLayer.Entities;

namespace Quickbooks.Entities.Import
{
    class Customer : Base
    {
        protected Intuit.Ipp.Data.Customer sourceData;
        protected bool hasLink;

        Clients client;

        public Customer(DBContext db) : base(db)
        {
        }

        public override void Convert()
        {
            var linkId = _version.FindLinkId(sourceData);

            client = _db.Clients.Where(c => c.Id == linkId || c.ClientCode == sourceData.Id).SingleOrDefault();

            if (client == null)
            {
                hasLink = false;
                client = new Clients{
                    ClientCode = sourceData.Id,
                    ClientEngagements = new ClientEngagements[] {
                        new ClientEngagements {
                            EngagementEnum = ClientsEngagement.Default,
                            StatusEnum = ClientEngagementsStatus.Active,
                            LastUpdate = DateTime.UtcNow
                        }
                    },
                    PersonOrCompany = new PersonOrCompany(),
                    OfficeId = _officeId
                };
            }
            else
            {
                hasLink = true;
            }

            client.StatusEnum = ClientsStatus.Active;
            client.LastUpdate = DateTime.UtcNow;

            if (client.PersonOrCompany == null)
            {
                client.PersonOrCompany = _db.PersonOrCompany.Where(c => c.Id == client.PersonOrCompanyId).SingleOrDefault();

                if (client.PersonOrCompany == null)
                {
                    client.PersonOrCompany = new PersonOrCompany();
                }
            }

            client.PersonOrCompany.LastUpdate = DateTime.Now;
            client.PersonOrCompany.TypeEnum   = EntitiesId.PersonOrCompany;
            client.PersonOrCompany.StatusEnum = PersonOrCompanyStatus.Active;
            client.PersonOrCompany.FirstName  = sourceData.GivenName;
            client.PersonOrCompany.MiddleName = sourceData.MiddleName;
            client.PersonOrCompany.LastName   = sourceData.FamilyName;

            client.PersonOrCompany.Title    = sourceData.Title;
            client.PersonOrCompany.JobTitle = sourceData.JobInfo?.Description;

            client.PersonOrCompany.PrintAs  = sourceData.PrintOnCheckName;

            client.PersonOrCompany.CompanyName = sourceData.CompanyName;

            client.PersonOrCompany.BusinessNum = sourceData.PrimaryPhone?.FreeFormNumber;
            client.PersonOrCompany.OtherNum    = sourceData.AlternatePhone?.FreeFormNumber;

            client.PersonOrCompany.WebPage     = sourceData.WebAddr?.URI;
        }

        public override void SetData(Intuit.Ipp.Data.IntuitEntity quickbooksEntity)
        {
            DestEntityType = "personOrCompany";
            sourceData = (Intuit.Ipp.Data.Customer)quickbooksEntity;
        }

        public override Guid Save()
        {
            // If found
            if (false == hasLink)
            {
                _db.Clients.Add(client);
            }
            else
            {
                // if not - insert
                _db.Clients.Update(client);
            }

            _db.SaveChanges();
            _version.UpdateLink(sourceData, client.Id);

            return client.Id;
        }
    }
}
