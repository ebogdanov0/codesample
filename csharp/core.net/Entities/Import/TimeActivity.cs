﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLayer;

namespace Quickbooks.Entities.Import
{
    class TimeActivity : Base
    {
        Intuit.Ipp.Data.TimeActivity timeActivity;

        DataLayer.Entities.Times timesRecord;

        public TimeActivity(DBContext db) : base(db)
        {
        }

        public override void SetData(Intuit.Ipp.Data.IntuitEntity quickbooksEntity)
        {
            timeActivity = (Intuit.Ipp.Data.TimeActivity) quickbooksEntity;
        }

        public override void Convert()
        {
            timesRecord = new DataLayer.Entities.Times();
            // timesRecord.ClientEngagementId = "";

            throw new NotImplementedException();
        }

        public override Guid Save()
        {
            // If found
            _db.Times.Update(timesRecord);
            // if not - insert
            _db.Times.Add(timesRecord);

            throw new NotImplementedException();
        }
    }
}
