﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DataLayer;
using Intuit.Ipp.Data;

namespace Quickbooks.Entities.Import
{
    class Account : Base
    {
        protected Intuit.Ipp.Data.Account sourceData;

        DataLayer.Entities.FirmBankAccounts dbRecord;

        public Account(DBContext db) : base(db)
        {   
        }

        public override void SetData(IntuitEntity quickbBooksEntity)
        {
            DestEntityType = "FirmBankAccounts";
            sourceData = (Intuit.Ipp.Data.Account) quickbBooksEntity;
        }

        public override void Convert()
        {
            var linkId = _version.FindLinkId(sourceData);

            dbRecord = _db.FirmBankAccounts.Where(c => c.Id == linkId).SingleOrDefault();

            if (dbRecord == null)
            {
                dbRecord = new DataLayer.Entities.FirmBankAccounts();
                dbRecord.Id = linkId;
            }

            dbRecord.Account = sourceData.FullyQualifiedName;
            dbRecord.Currency = sourceData.CurrencyRef?.Value;
            dbRecord.Description = sourceData.Description;
        }

        public override Guid Save()
        {
            // If found
            if (_version.HasLink(sourceData))
            {
                _db.FirmBankAccounts.Add(dbRecord);
            }
            else
            {
                _db.FirmBankAccounts.Update(dbRecord);
            }

            _db.SaveChanges();
            _version.UpdateLink(sourceData, dbRecord.Id);

            return dbRecord.Id;
        }
    }
}
