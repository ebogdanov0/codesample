﻿using System;
using System.Collections.Generic;
using System.Text;
using Intuit.Ipp.Data;
using DataLayer;

namespace Quickbooks.Entities.Import
{
    class Invoice : Base
    {
        Intuit.Ipp.Data.Invoice invoice;
        DataLayer.Entities.Invoices invoiceRecord;

        public Invoice(DBContext db) : base(db)
        {
        }

        public override void Convert()
        {
            invoiceRecord = new DataLayer.Entities.Invoices();
            invoiceRecord.Amount = invoice.TotalAmt;
            invoices.InvoiceNum = invoice.Id;
            invoiceRecord.BillDate = invoice.DueDate;

            // @todo Lookup client id by link,  invoices.ClientEngagementId
            // @todo Lookup Tax by id invoices.Tax1 = invoice.TxnTaxDetail.TxnTaxCodeRef.Value;

            throw new NotImplementedException();
        }

        public override Guid Save()
        {
            // If found
            if (_version.HasLink(invoice))
            {
                _db.Invoices.Update(invoiceRecord);
            }
            else
            {
                _db.Invoices.Add(invoiceRecord);
            }
            
            return invoiceRecord.Id;
        }

        public override void SetData(IntuitEntity quickbooksEntity)
        {
            invoice = (Intuit.Ipp.Data.Invoice)quickbooksEntity;
        }
    }
}
